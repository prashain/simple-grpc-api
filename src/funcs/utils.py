def is_empty(input):
    return input != None and len(input.strip())>0

def not_null(input):
    return input != None