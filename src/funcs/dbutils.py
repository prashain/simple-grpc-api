import MySQLdb

# Externalise db connection
def connect():
    return MySQLdb.connect(host="127.0.0.1", port=3306, user="root", passwd="admin", db="cbo")

def close(conn):
    try:
        if(type(conn) == "Connection"):
            conn.close()
        else:
            print("close can be called on MySQLConnection object")
    except Exception:
         print("Error while closing connection")