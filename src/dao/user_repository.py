import sys
sys.path.append("..")
from src.funcs.dbutils import connect,close
from src.model.user import User
def create_user(user_id,username,first_name,last_name,email):
    connection = connect()
    cursor = connection.cursor()
    create_sql = "insert into user values({},\"{}\",\"{}\",\"{}\",\"{}\")".format(user_id,username,first_name,last_name,email)
    print(create_sql)
    cursor.execute(create_sql)
    connection.commit()
    connection.close()

def create_user1(user):
    connection = connect()
    cursor = connection.cursor()
    create_sql = "insert into user values({},\"{}\",\"{}\",\"{}\",\"{}\")".format(user.id,user.username,user.first_name,user.last_name,user.email)
    print(create_sql)
    cursor.execute(create_sql)
    connection.commit()
    connection.close()

def fetchbyid(id):
    try:
        connection = connect()
        cursor = connection.cursor()
        cursor.execute("select * from user where user_id={}".format(id))
        row = cursor.fetchone()
        user = User(row[0],row[1],row[2],row[3],row[4])
        return user
    except connection.Error as e:
        print("Error reading data from MySQL table", e)
    finally:
        connection.close()
        print("Connection closed")

def fetchall():
    try:
        connection = connect()
        cursor = connection.cursor()
        cursor.execute("select * from user")
        rows = cursor.fetchall()
        result = [User(row[0],row[1],row[2],row[3],row[4]) for row in rows]
        #[]
        #for row in rows:

       # user = User(row[0],row[1],row[2],row[3],row[4])
        return result
    except connection.Error as e:
        print("Error reading data from MySQL table", e)
    finally:
        connection.close()
        print("Connection closed")

def delete_user(id):
    try:
        connection = connect()
        cursor = connection.cursor()
        cursor.execute("delete from user where user_id={}".format(id))
        connection.commit()
    except connection.Error as e:
        print("Error while deleting user with id {}".format(id))
    finally:
        connection.close()



