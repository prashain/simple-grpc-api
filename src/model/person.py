from pydantic import BaseModel
class Person(BaseModel):
    id: int
    username: str
    first_name: str
    last_name:  str
    email:  str