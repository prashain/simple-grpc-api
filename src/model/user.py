class User:
    def __init__(self,id,username,first_name,last_name,email):
        self.id=id
        self.username=username
        self.first_name=first_name
        self.last_name= last_name
        self.email= email

    def __str__(self):
        return "User[id={} username={} name={} {} email ={}]".format(self.id,self.username,self.first_name,self.last_name,self.email)