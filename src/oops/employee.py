

import sys
sys.path.append("..")
from funcs.utils import is_empty;
class Employee:
    def __init__(self,first_name, last_name,status):
        self.first_name = first_name
        self.last_name = last_name
        self.status = status
    
    def is_active(self):
        return self.status=='ATV'

    def __str__(self):
        return "Employee[firstname={} lastname={} status={}]".format(self.first_name,self.last_name,self.status)

if __name__ == '__main__':
     emp = Employee("prashant","yadav","LAC")
     print(emp.is_active())
     print(is_empty(emp))
    # print(funcs.utils.is_empty(emp))