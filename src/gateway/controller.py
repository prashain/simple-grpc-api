# Entry to the api endpoints
import sys
sys.path.append("..")
from fastapi import FastAPI,Response
from src.dao.user_repository import fetchbyid,create_user1,delete_user
from src.model.person import Person
app = FastAPI()

@app.get('/')
async def index():
    return {"Real": "Python"}

@app.get('/users/{id}')
async def fetchuser(id):
    return fetchbyid(id)

@app.post('/user', status_code=201)
async def add_person(payload: Person):
    create_user1(payload)
    return {'id': payload.id}


@app.delete('/user',status_code=204)
async def delete_person(user_id: int):
    print("deleteing user with userId {}".format(user_id))
    delete_user(user_id)
    return Response(status_code=204)