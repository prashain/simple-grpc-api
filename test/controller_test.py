# """
#     Test for api controller
# """
import sys
sys.path.append("..")
from fastapi import FastAPI
from fastapi.testclient import TestClient

from src.gateway.controller import app
from src.dao.user_repository import fetchbyid,create_user1,delete_user
from src.model.person import Person




client = TestClient(app)


def test_read_main():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"Real": "Python"}


def test_get_user():
    response = client.get("/user/1")
    assert response.status_code == 200
    assert response.json() == {"id":1,"username":"prayadav","first_name":"Prashant","last_name":"Yadav","email":"prayadav@gmail.com"}