# FASTAPI implementation for an api

## Dependencies
-   fastapi 
-   mysql (for database connectivity)
-   uvicorn
-   pydantic
-   asyncio
-   grpclib
-   protobuf
- pytest for running tests

## How to run
### Application:
Depends on python version >3. Best bet is to use virtualenv 
.   python3 -m venv env
.   source env/bin/activate
 Execute on a terminal
` python main.py` 
#### Setting up mysql database
. Run user.sql to set up database
 

### Running tests
From a terminal window navigate to test directory and execute
`pytest`

### GRPC services
The api exposes two endpoints for enabling clients to make grpc requests for
1.   Finding users by id.
2.   Finding all users

While 1. is a grpc unary operation, 2) is a grpc server stream operation. The proto file is located in #(src/grpc)

### Generating proto stubs
`python3 -m grpc_tools.protoc -I ./grpc --python_out=./grpc --grpclib_python_out=./grpc ./grpc/usersvc.proto`




